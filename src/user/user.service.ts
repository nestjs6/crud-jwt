import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  getUserAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  getUserId(id: number) {
    return this.usersRepository.findOne({ where: { id } });
  }

  postUser(createUserDto: CreateUserDto) {
    return this.usersRepository.save(createUserDto);
  }

  putUser(updateUserDto: UpdateUserDto, id: number) {
    console.log(updateUserDto);

    return this.usersRepository.update(id, updateUserDto);
  }

  deleteUser(id: number) {
    return this.usersRepository.delete(id);
  }

  findByEmail(email: string) {
    return this.usersRepository.findOne({ where: { email } });
  }
}
