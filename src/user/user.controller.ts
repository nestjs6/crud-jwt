import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { UpdateUserDto } from 'src/user/dto/update-user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  // TODO: OK
  @Get()
  getUserAll() {
    return this.userService.getUserAll();
  }

  // TODO: OK
  @Get(':id')
  getUserId(@Param('id', ParseIntPipe) id: number) {
    return this.userService.getUserId(id);
  }

  // TODO: OK
  @Post()
  postUser(@Body() createUserDto: CreateUserDto) {
    return this.userService.postUser(createUserDto);
  }

  // TODO: OK
  @Put(':id')
  putUser(
    @Body() updateUserDto: UpdateUserDto,
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.userService.putUser(updateUserDto, id);
  }

  // TODO: OK
  @Delete(':id')
  deleteUser(@Param('id', ParseIntPipe) id: number) {
    return this.userService.deleteUser(id);
  }
}
