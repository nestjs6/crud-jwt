import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  postAuth(@Request() req) {
    console.log(req.user);

    return this.authService.login(req.user);
  }

  //   routes protected
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
  //   postAuth(@Body() createAuthDto: any) {
  //    return this.authService.validateUser(
  //      createAuthDto.email,
  //      createAuthDto.password,
  //    );
  //  }
}
